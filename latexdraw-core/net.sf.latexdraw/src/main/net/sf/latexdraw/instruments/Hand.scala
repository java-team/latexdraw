/*
 * This file is part of LaTeXDraw
 * Copyright (c) 2005-2016 Arnaud BLOUIN
 *  LaTeXDraw is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *  LaTeXDraw is distributed without any warranty; without even the
 *  implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 *  PURPOSE. See the GNU General Public License for more details.
 */
package net.sf.latexdraw.instruments

import java.awt.Cursor
import java.awt.event.{KeyEvent, MouseEvent}
import java.awt.geom.Rectangle2D
import java.util.ArrayList

import net.sf.latexdraw.actions.shape._
import net.sf.latexdraw.badaboom.BadaboomCollector
import net.sf.latexdraw.glib.models.ShapeFactory
import net.sf.latexdraw.glib.models.ShapeFactory._
import net.sf.latexdraw.glib.models.interfaces.shape.{IPlot, IPoint, IShape, IText}
import net.sf.latexdraw.glib.ui.{ICanvas, LCanvas}
import net.sf.latexdraw.glib.views.Java2D.interfaces.{IViewPlot, IViewShape, IViewText}
import org.malai.action.Action
import org.malai.instrument.{Instrument, InteractorImpl}
import org.malai.interaction.Interaction
import org.malai.mapping.MappingRegistry
import org.malai.swing.action.library.{ActivateInactivateInstruments, MoveCamera}
import org.malai.swing.interaction.library.{DoubleClick, _}

import scala.collection.JavaConversions.asScalaBuffer
import scala.collection.mutable.Buffer


/**
 * This instrument allows to manipulate (e.g. move or select) shapes.
 * @author Arnaud BLOUIN
 */
class Hand(canvas : ICanvas, val textSetter : TextSetter) extends CanvasInstrument(canvas) {
	var _metaCustomiser : MetaShapeCustomiser = _

	override protected def initialiseInteractors() {
		try{
			addInteractor(new Press2Select(this))
			addInteractor(new DnD2Select(this))
			addInteractor(new DnD2Translate(this))
			addInteractor(new DnD2MoveViewport(canvas, this))
			addInteractor(new DoubleClick2InitTextSetter(this))
			addInteractor(new F2ToInitTextSetter(this))
			addInteractor(new CtrlA2SelectAllShapes(this))
			addInteractor(new CtrlU2UpdateShapes(this))
			addInteractor(new Press2SetText(this))
			addInteractor(new Press2DeactivateTextSetter(this))
		}catch{case ex: Throwable => BadaboomCollector.INSTANCE.add(ex)}
	}


	override def setActivated(activated : Boolean) {
		if(this.activated!=activated)
			super.setActivated(activated)
	}


	override def interimFeedback() {
		// The rectangle used for the interim feedback of the selection is removed.
		canvas.setTempUserSelectionBorder(null)
		canvas.setCursor(Cursor.getDefaultCursor)
	}

	def setMetaCustomiser(metaCustomiser:MetaShapeCustomiser) { _metaCustomiser = metaCustomiser }


	override def onActionDone(action:Action) {
		action match {
			case _:TranslateShapes => _metaCustomiser.dimPosCustomiser.update()
			case _:ActivateInactivateInstruments => canvas.requestFocus()
			case _ =>
		}
	}
}

private sealed class Press2SetText(ins:Hand) extends InteractorImpl[ModifyShapeProperty, Press, Hand](ins, false, classOf[ModifyShapeProperty], classOf[Press]) {
	override def initAction {
		action.setGroup(ShapeFactory.createGroup(instrument.textSetter.text))
		action.setProperty(ShapeProperties.TEXT)
		action.setValue(instrument.textSetter.textField.getText())
	}

	override def isConditionRespected = instrument.textSetter.isActivated && !instrument.textSetter.getTextField.getText.isEmpty
}

private sealed class Press2DeactivateTextSetter(ins:Hand) extends InteractorImpl[ActivateInactivateInstruments, Press, Hand](ins, false, classOf[ActivateInactivateInstruments], classOf[Press]) {
	def initAction {
		action.addInstrumentToInactivate(instrument.textSetter)
	}

	def isConditionRespected: Boolean = {
		return instrument.textSetter.textField.isValidText && !instrument.textSetter.textField.getText.isEmpty
	}
}

private sealed class CtrlU2UpdateShapes(ins:Hand) extends InteractorImpl[UpdateToGrid, KeysPressure, Hand](ins, false, classOf[UpdateToGrid], classOf[KeysPressure]) {
	override def initAction() {
		action.setShape(instrument.canvas.getDrawing.getSelection.duplicateDeep(false))
		action.setGrid(instrument.canvas.getMagneticGrid)
	}

	override def isConditionRespected = instrument.canvas.getMagneticGrid.isMagnetic &&
		interaction.getKeys.size==2 && interaction.getKeys.contains(KeyEvent.VK_U) && interaction.getKeys.contains(KeyEvent.VK_CONTROL)
}


private sealed class CtrlA2SelectAllShapes(ins:Hand) extends InteractorImpl[SelectShapes, KeysPressure, Hand](ins, false, classOf[SelectShapes], classOf[KeysPressure]) {
	override def initAction() {
		instrument.canvas.getDrawing.getShapes.foreach{sh => action.addShape(sh)}
		action.setDrawing(instrument.canvas.getDrawing)
	}

	override def isConditionRespected =
		interaction.getKeys.size==2 && interaction.getKeys.contains(KeyEvent.VK_A) && interaction.getKeys.contains(KeyEvent.VK_CONTROL)
}

private abstract sealed class Interaction2InitTextSetter[I <: Interaction](ins:Hand, clazz:Class[I])
							extends InteractorImpl[InitTextSetter, I, Hand](ins, false, classOf[InitTextSetter], clazz) {
	protected def setAction(sh:Object) {
		var pos:Option[IPoint] = None

		sh match {
			case plot:IPlot =>
				action.setPlotShape(plot)
				pos = Some(plot.getPosition)
			case txt:IText =>
				action.setTextShape(txt)
				pos = Some(txt.getPosition)
			case _ =>
		}

		pos match {
			case Some(position) =>
				val screen = instrument.canvas.asInstanceOf[LCanvas].getVisibleRect
				val zoom = instrument.canvas.getZoom
				val x = instrument.canvas.getOrigin.getX - screen.getX + position.getX * zoom
				val y = instrument.canvas.getOrigin.getY - screen.getY + position.getY * zoom
				action.setInstrument(instrument.textSetter)
				action.setTextSetter(instrument.textSetter)
				action.setAbsolutePoint(ShapeFactory.createPoint(x, y))
				action.setRelativePoint(ShapeFactory.createPoint(position))
			case None =>
		}
	}
}

private sealed class F2ToInitTextSetter(ins:Hand) extends Interaction2InitTextSetter[KeysPressure](ins, classOf[KeysPressure]) {
	override def initAction() {
		setAction(instrument.canvas.getDrawing.getSelection.getShapeAt(0))
	}

	override def isConditionRespected:Boolean = interaction.getKeys.size()==1 && interaction.getKeys.get(0)==KeyEvent.VK_F2 &&
			instrument.canvas.getDrawing.getSelection.size()==1 &&
			(instrument.canvas.getDrawing.getSelection.getShapeAt(0).isInstanceOf[IText] ||
			instrument.canvas.getDrawing.getSelection.getShapeAt(0).isInstanceOf[IPlot])
}

private sealed class DoubleClick2InitTextSetter(ins : Hand) extends Interaction2InitTextSetter[DoubleClick](ins, classOf[DoubleClick]) {
	override def initAction() {
		interaction.getTarget match {
			case shape: IViewShape => setAction(shape.getShape)
			case _ =>
		}
	}

	override def isConditionRespected = interaction.getTarget.isInstanceOf[IViewText] || interaction.getTarget.isInstanceOf[IViewPlot]
}



/**
 * This link allows to translate the selected shapes.
 */
private sealed class DnD2Translate(hand : Hand) extends InteractorImpl[TranslateShapes, DnD, Hand](hand, true, classOf[TranslateShapes], classOf[DnD]) {
	override def initAction() {
		action.setDrawing(instrument.canvas.getDrawing)
		action.setShape(instrument.canvas.getDrawing.getSelection.duplicateDeep(false))
	}


	override def updateAction() {
		val startPt	= instrument.getAdaptedGridPoint(interaction.getStartPt)
		val endPt	= instrument.getAdaptedGridPoint(interaction.getEndPt)

		action.setTx(endPt.getX - startPt.getX)
		action.setTy(endPt.getY - startPt.getY)
	}

	override def isConditionRespected: Boolean = {
		val startObject = interaction.getStartObject
		val button 		= interaction.getButton
		return  !instrument.canvas.getDrawing.getSelection.isEmpty &&
				(startObject==instrument.canvas && button==MouseEvent.BUTTON3 ||
				 startObject.isInstanceOf[IViewShape] && (button==MouseEvent.BUTTON1 || button==MouseEvent.BUTTON3))
	}


	override def interimFeedback() {
		super.interimFeedback()
		instrument.canvas.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR))
	}
}



private sealed class Press2Select(ins : Hand) extends InteractorImpl[SelectShapes, PressWithKeys, Hand](ins, false, classOf[SelectShapes], classOf[PressWithKeys]) {
	override def initAction() {
		action.setDrawing(instrument.canvas.getDrawing)
	}

	override def updateAction() {
		val target = interaction.getTarget

		if(target.isInstanceOf[IViewShape]) {
			val keys = interaction.getKeys
			val selection = instrument.canvas.getDrawing.getSelection.getShapes
			val targetSh = MappingRegistry.REGISTRY.getSourceFromTarget(target, classOf[IShape])

			if(keys.contains(KeyEvent.VK_SHIFT))
				selection.filter{sh => sh!=targetSh}.foreach{sh => action.addShape(sh)}
			else if(keys.contains(KeyEvent.VK_CONTROL)) {
				selection.foreach{sh => action.addShape(sh)}
				action.addShape(targetSh)
			}
			else
				action.setShape(targetSh)
		}
	}

	override def isConditionRespected: Boolean = interaction.getTarget.isInstanceOf[IViewShape] &&
      !instrument.canvas.getDrawing.getSelection.contains(MappingRegistry.REGISTRY.getSourceFromTarget(interaction.getTarget.asInstanceOf[IViewShape], classOf[IShape]))
}



/**
 * This link allows to select several shapes.
 */
private sealed class DnD2Select(hand : Hand) extends InteractorImpl[SelectShapes, DnDWithKeys, Hand](hand, true, classOf[SelectShapes], classOf[DnDWithKeys]) {
	/** The is rectangle is used as interim feedback to show the rectangle made by the user to select some shapes. */
	val selectionBorder : Rectangle2D = new Rectangle2D.Double()
	var selectedShapes : Buffer[IShape] = _
	var selectedViews  : Buffer[IViewShape] = _

	override def initAction() {
		action.setDrawing(instrument.canvas.getDrawing)
		selectedShapes = new ArrayList(instrument.canvas.getDrawing.getSelection.getShapes)
		selectedViews  = instrument.canvas.getBorderInstrument.selection.clone
	}


	override def updateAction() {
		val start = instrument.getAdaptedOriginPoint(interaction.getPoint)
		val end	  = instrument.getAdaptedOriginPoint(interaction.getEndPt)
		val minX  = math.min(start.getX, end.getX)
		val maxX  = math.max(start.getX, end.getX)
		val minY  = math.min(start.getY, end.getY)
		val maxY  = math.max(start.getY, end.getY)
		val zoom  = instrument.canvas.getZoom
		val keys = interaction.getKeys

		// Updating the rectangle used for the interim feedback and for the selection of shapes.
		selectionBorder.setFrame(minX, minY, Math.max(maxX-minX, 1), Math.max(maxY-minY, 1))
		// Cleaning the selected shapes in the action.
		action.setShape(null)

		if(keys.contains(KeyEvent.VK_SHIFT))
			selectedViews.filter{view => !view.intersects(selectionBorder)}.foreach{
					view => action.addShape(MappingRegistry.REGISTRY.getSourceFromTarget(view, classOf[IShape]))}
		else {
			if(keys.contains(KeyEvent.VK_CONTROL))
				selectedShapes.foreach{sh => action.addShape(sh)}
			if(!selectionBorder.isEmpty)
				instrument.canvas.getViews.foreach{view =>
					if(view.intersects(selectionBorder))
						// Taking the shape in function of the view.
						action.addShape(MappingRegistry.REGISTRY.getSourceFromTarget(view, classOf[IShape]))
				}
		}
	}

	override def isConditionRespected = interaction.getStartObject==instrument.canvas && interaction.getButton==MouseEvent.BUTTON1

	override def interimFeedback() {
		instrument.canvas.setTempUserSelectionBorder(selectionBorder)
		instrument.canvas.refresh
	}
}


/**
 * Moves the viewport using the hand.
 */
class DnD2MoveViewport(canvas:ICanvas, ins:Instrument) extends InteractorImpl[MoveCamera, DnD, Instrument](ins, true, classOf[MoveCamera], classOf[DnD]) {
	override def initAction() {
		action.setScrollPane(canvas.getScrollpane)
	}

	override def updateAction() {
		val startPt	= interaction.getStartPt
		val endPt	= interaction.getEndPt
		val pane	= canvas.getScrollpane
		action.setPx(pane.getHorizontalScrollBar.getValue+pane.getHorizontalScrollBar.getWidth/2+(startPt.getX - endPt.getX).toInt)
		action.setPy(pane.getVerticalScrollBar.getValue+pane.getVerticalScrollBar.getHeight/2+(startPt.getY - endPt.getY).toInt)
	}

	override def isConditionRespected = interaction.getButton==MouseEvent.BUTTON2

	override def interimFeedback() {
		super.interimFeedback
		canvas.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR))
	}
}